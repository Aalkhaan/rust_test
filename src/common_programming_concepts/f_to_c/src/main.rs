use std::io;

fn main() {
    let mut fahrenheit = String::new();
    println!("Enter a fahrenheit temperature");
    io::stdin()
        .read_line(&mut fahrenheit)
        .expect("Cannot read line");
    let fahrenheit: f64 = fahrenheit.trim().parse()
        .expect("Please enter a float number");
    println!("{} F° is {} C°", fahrenheit, (fahrenheit - 32.0) / 1.8);
}
