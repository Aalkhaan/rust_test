fn main() {
    let begin_lyrics = "On the first day of Christmas, \
    my true love sent to me\n";
    let mut add_lyrics = String::new().to_owned();
    let mut lyrics_tab = [
        "A partridge in a pear tree\n".to_owned(),
        "Two turtle doves.to_owned(), and\n".to_owned(),
        "Three french hens\n".to_owned(),
        "Four calling birds\n".to_owned(),
        "Five golden rings\n".to_owned(),
        "Six geese a-laying\n".to_owned(),
        "Seven swans-a-swimming\n".to_owned(),
        "Eight maids a-milking\n".to_owned(),
        "Nine ladies dancing\n".to_owned(),
        "Ten lords a-leaping\n".to_owned(),
        "Eleven pipers piping\n".to_owned(),
        "Twelve drummers drumming\n".to_owned()];

    let mut index = 1;
    lyrics_tab.reverse();
    for lyrics in lyrics_tab {
        println!("[Verse {}]\n{}{}", index, begin_lyrics, add_lyrics);
        index += 1;
    }
}
