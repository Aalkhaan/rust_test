fn main() {
    const SEQUENCE: [u8; 6] = [1, 2, 3, 5, 8, 13];
    let mut second_to_last = 0;
    let mut last = 1;

    let n = 3;

    if n == 0 || n == 1 {
        println!("nth term is: {}", n)
    } else {
        for nth in SEQUENCE {
            if iter_fibonacci(nth, &mut second_to_last, &mut last) {
                println!("error");
                break
            }
        }
    }
}

fn iter_fibonacci(nth: u8, a: &mut u8, b: &mut u8) -> bool {
    let new_last = *a + *b;
    *a = *b;
    *b = new_last;

    nth != *b
}
