fn cree_matrice() -> Vec<Vec<u32>> {
    vec![vec![1, 2], vec![3, 4]]
}

fn produit_scalaire(t1: &[f64], t2: &[f64]) -> f64 {
    t1.iter().zip(t2).map(|(a, b)| *a * *b).sum()
}

fn main() {
    let v1 = vec![1.0, 2.0, 3.0];
    let v2 = vec![4.0, 5.0, 6.0];
    println!("{}", produit_scalaire(&v1, &v2));
}
