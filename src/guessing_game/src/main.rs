use std::cmp::Ordering;
use std::io;

use rand::Rng;

fn main() {
    println!("Guessing game");

    let secret_number = rand::thread_rng().gen_range(1..101);

    println!("Find the secret number between 1 and 100 (inclusive)");

    loop {
        let mut guess = String::new();
        println!("Enter your guess");
        io::stdin().read_line(&mut guess).expect("Cannot read line");
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Please enter a number");
                continue;
            },
        };

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small"),
            Ordering::Equal => {
                println!("You won!");
                break;
            }
            Ordering::Greater => println!("Too big!"),
        }
    }
}